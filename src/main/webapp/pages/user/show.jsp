<%--
  Created by IntelliJ IDEA.
  User: dhiraj
  Date: 6/26/19
  Time: 9:48 PM
  To change this template use File | Settings | File Templates.
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>

<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>

<t:mainlayout title="user details">
    <jsp:body>
        <section class="content-header">
            <h1>
                User Show
                <small>User Details</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="${pageContext.request.contextPath}/dashboard"><i class="fa fa-dashboard"></i> Dashboard</a>
                </li>
                <li><a href="${pageContext.request.contextPath}/user/list"><i class="fa fa-users"></i> User List</a>
                </li>
                <li class="active"><i class="fa fa-user"></i> User Show</li>
            </ol>
        </section>
        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-lg-12">
                    <c:if test="${not empty message}">
                        <div class="alert alert-success alert-dismissable">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">x</a>
                            <strong>${message}</strong>
                        </div>
                    </c:if>

                    <c:if test="${not empty error}">
                        <div class="alert alert-danger alert-dismissable">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">x</a>
                            <strong>${error}</strong>
                        </div>
                    </c:if>
                </div>
            </div>

            <div class="row">
                <div class="col-lg-12">
                    <div class="box box-info">

                        <div class="box-header with-border">
                            <a class="btn btn-primary btn-sm pull-right" href="${pageContext.request.contextPath}/user/edit/${user.userId}"><i class="fa fa-edit"></i>  Edit User</a>
                        </div>

                        <div class="box-body">
                            <div class="row">
                                <div class="col-lg-12">
                                    <strong>User</strong>

                                    <p class="text-muted">
                                            ${user.email}
                                    </p>

                                    <hr>

                                    <strong>User Type</strong>

                                    <p class="text-muted">
                                            ${user.userType}
                                    </p>

                                    <hr>

                                    <strong>isEnabled</strong>

                                    <p class="text-muted">
                                            ${user.enabled}
                                    </p>

                                    <hr>

                                    <strong>isAccountNonExpired</strong>

                                    <p class="text-muted">${user.accountNonExpired}</p>

                                    <hr>

                                    <strong>isAccountNonLocked</strong>

                                    <p class="text-muted">${user.accountNonLocked}</p>

                                    <hr>

                                    <strong>Expired On Date </strong>

                                    <p class="text-muted"><fmt:formatDate pattern="yyyy-MMM-dd HH:mm:ss" value="${user.expire}"/></p>

                                    <hr>
                                        <%--
                                      <strong><i class="fa fa-pencil margin-r-5"></i> Roles</strong>

                                        <p>
                                              <c:forEach var="role" items="${user.roleSet}">
                                                  <span class="label label-success">${role.title}</span>
                                              </c:forEach>
                                          </p>

                                          <hr>--%>
                                    <hr>
                                </div>
                            </div>

                        </div>

                    </div>
                </div>
            </div>

        </section>
    </jsp:body>

</t:mainlayout>