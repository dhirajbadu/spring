package org.account.config;

import org.springframework.web.servlet.support.AbstractAnnotationConfigDispatcherServletInitializer;

import java.util.HashMap;
import java.util.Map;

/**
 * @author Dhiraj
 */
public class AppInitializer extends AbstractAnnotationConfigDispatcherServletInitializer {

    private static final Map<String , Class[]> configClasses = new HashMap() {{
        put("RootConfigClasses" , new Class[] { PersistenceJPAConfig.class , WebSecurityConfig.class});
        put("ServletConfigClasses" , new Class[] { WebMvcConfig.class});
    }};

    @Override
    protected String[] getServletMappings() {
        return new String[] {
                "/"
        };
    }

    @Override
    protected Class<?>[] getRootConfigClasses() {
        return configClasses.get("RootConfigClasses");
    }

    @Override
    protected Class<?>[] getServletConfigClasses() {
        return configClasses.get("ServletConfigClasses");
    }
}

/*
* https://www.javaguides.net/2018/11/spring-mvc-5-spring-data-jpa-hibernate-jsp-mysql-tutorial.html
* */
