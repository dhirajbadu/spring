package org.account.config;

import org.account.commonUtls.DateParseUtil;
import org.account.core.enumconstants.Permission;
import org.account.core.enumconstants.Status;
import org.account.core.enumconstants.UserType;
import org.account.core.modal.entity.RoleEntity;
import org.account.core.modal.entity.UserEntity;
import org.account.core.modal.repositories.RoleRepository;
import org.account.core.modal.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.HashSet;
import java.util.Set;

@Service
public class StartUpLoader {

    @Autowired
    private RoleRepository roleRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    public void load(){
        createUser();
    }

    @PostConstruct
    public void createUser(){
        UserEntity userEntity = userRepository.findByEmail("system@gmail.com");

        if (userEntity != null){
            return;
        }

        userEntity = new UserEntity();

        userEntity.setEmail("system@gmail.com");
        userEntity.setAccountNonExpired(true);
        userEntity.setAccountNonLocked(true);
        userEntity.setEnabled(true);
        userEntity.setUserType(UserType.ADMIN);
        userEntity.setExpire(DateParseUtil.addYears(DateParseUtil.getCurrentDateTime() , 1));
        userEntity.setPassword(passwordEncoder.encode("123456789"));

        Set<RoleEntity> roleEntities = new HashSet<>();

        roleEntities.add(createRole());

        userEntity.setRoleSet(roleEntities);

        userRepository.save(userEntity);
    }

    public RoleEntity createRole(){

        RoleEntity roleEntity = roleRepository.findByTitle("System");

        if (roleEntity != null){
            return roleEntity;
        }

        roleEntity = new RoleEntity();

        Set<Permission> permissionSet = new HashSet<>();

        permissionSet.add(Permission.USER_CREATE);
        permissionSet.add(Permission.USER_UPDATE);
        permissionSet.add(Permission.USER_VIEW);

        roleEntity.setTitle("System");
        roleEntity.setStatus(Status.ACTIVE);
        roleEntity.setPermissionSet(permissionSet);

        return roleRepository.save(roleEntity);
    }
}
