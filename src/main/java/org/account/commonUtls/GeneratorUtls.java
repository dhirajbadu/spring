package org.account.commonUtls;

public class GeneratorUtls {

    public static String randomNumericString(int length) {

        String charString = Long.toString(System.currentTimeMillis());
        String[] arrayString = charString.split("");
        StringBuilder randomCode = new StringBuilder();
        for (int i = 0; i < length; i++) {
            int randomIndex = (int) (Math.random() * charString.length());
            randomCode.append(arrayString[randomIndex]);
        }

        LoggerUtil.logInfo("generated code >>>> " + randomCode);
        return randomCode.toString();
    }
}
