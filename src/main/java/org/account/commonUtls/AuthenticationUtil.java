package org.account.commonUtls;


import org.account.core.modal.dto.UserDto;
import org.account.core.modal.repositories.UserRepository;
import org.account.web.session.UserDetailsWrapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

@Component
public class AuthenticationUtil {

    @Autowired
    private UserRepository userRepository;

    public static final Object getCurrentPrincipal() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication == null) {
            return null;
        }
        Object principal = authentication.getPrincipal();

        return principal;
    }

    public static final UserDto getCurrentUser() {

        Object principal = getCurrentPrincipal();

        if (principal instanceof UserDetailsWrapper) {
            UserDto userDTO = ((UserDetailsWrapper) principal).getUser();
            return userDTO;
        }

        return null;
    }

}
