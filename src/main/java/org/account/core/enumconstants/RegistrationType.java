package org.account.enumconstants;

import java.util.Arrays;
import java.util.List;

public enum  RegistrationType {
    PAN("Pan" , "001"), VAT("Vat" , "002"), PAN_VAT("Pan_Vat" , "003");

    private final String value;

    private final String code;

    RegistrationType(String value , String code) {
        this.value = value;
        this.code = code;
    }

    @Override
    public String toString() {
        return value + "-" + code;
    }

    public String getValue() {
        return value;
    }

    public String getCode() {
        return code;
    }

    public static RegistrationType getByValue(String value) {
        if (value == null)
            throw new IllegalArgumentException();
        for (RegistrationType v : values())
            if (value.equalsIgnoreCase(v.getValue()))
                return v;
        throw new IllegalArgumentException();
    }

    public static RegistrationType getByCode(String code) {
        if (code == null)
            throw new IllegalArgumentException();
        for (RegistrationType v : values())
            if (code.equalsIgnoreCase(v.getCode()))
                return v;
        throw new IllegalArgumentException();
    }

    public static List<RegistrationType> getEnumList(RegistrationType ...registrationTypes){
        return Arrays.asList(registrationTypes);
    }
}