package org.account.core.service.iservices;

import org.account.core.modal.dto.UserDto;
import org.account.core.modal.dto.filterdto.UserFilterDto;
import org.json.JSONException;
import org.springframework.transaction.annotation.Transactional;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

public interface IUserService {

    UserDto save(UserDto userDto);

    UserDto getById(long userId);

    UserDto getByIdJoinRole(long userId);

    UserDto getByEmail(String email);

    UserDto update(UserDto userDto);

    UserDto changePassword(String email , String password);

    @Transactional
    void changePassword(long userId, String newPassword,
                        HttpServletRequest request, HttpServletResponse response)
            throws IOException, JSONException;

    UserDto changePassword(long userId , String password);

    List<UserDto> list(int max, int offset, String direction, String property);

    List<UserDto> filter(UserFilterDto filterDTO);

    long countFilter(UserFilterDto filterDTO);

    long count();
}
