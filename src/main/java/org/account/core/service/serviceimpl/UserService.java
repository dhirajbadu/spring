package org.account.core.service.serviceimpl;

import org.account.commonUtls.ConvertUtil;
import org.account.commonUtls.ParseUtls;
import org.account.core.modal.converter.UserConverter;
import org.account.core.modal.dto.UserDto;
import org.account.core.modal.dto.filterdto.UserFilterDto;
import org.account.core.modal.entity.UserEntity;
import org.account.core.modal.repositories.UserRepository;
import org.account.core.modal.specification.UserSpecification;
import org.account.core.service.iservices.IUserService;
import org.json.JSONException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@Service
public class UserService implements IUserService{

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private UserConverter userConverter;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Override
    public UserDto save(UserDto userDto) {
        UserEntity userEntity = userConverter.convertToEntity(userDto);

        userEntity = userRepository.save(userEntity);

        return userConverter.convertToDto(userEntity);
    }

    @Override
    public UserDto getById(long userId) {
        return userConverter.convertToDto(userRepository.findById(userId));
    }

    @Override
    public UserDto getByIdJoinRole(long userId) {
        return userConverter.convertToDto(userRepository.findByIdJoinRole(userId));
    }

    @Override
    public UserDto getByEmail(String email) {
        return userConverter.convertToDto(userRepository.findByEmail(email));
    }

    @Override
    public UserDto update(UserDto userDto) {
        UserEntity userEntity = userRepository.findById((long)userDto.getUserId());

        userEntity = userConverter.copyConvertToEntity(userDto , userEntity);

        userEntity = userRepository.save(userEntity);

        return userConverter.convertToDto(userEntity);
    }

    private UserEntity changePassword(UserEntity userEntity , String password) {

        if (userEntity == null){
            return null;
        }

        userEntity.setPassword(passwordEncoder.encode(ParseUtls.trimString(password)));

        userEntity = userRepository.save(userEntity);

        return userEntity;
    }

    @Override
    public UserDto changePassword(String email, String password) {
        UserEntity userEntity = userRepository.findByEmail(email);

        userEntity = changePassword(userEntity , password);

        return userConverter.convertToDto(userEntity);
    }

    @Override
    @Transactional
    public void changePassword(long userId, String newPassword,
                               HttpServletRequest request, HttpServletResponse response)
            throws IOException, JSONException {
        changePassword(userId, newPassword);

        Authentication auth = SecurityContextHolder.getContext()
                .getAuthentication();

        if (auth != null) {
            new SecurityContextLogoutHandler().logout(request, response, auth);
        }
    }

    @Override
    public UserDto changePassword(long userId, String password) {
        UserEntity userEntity = userRepository.findById(userId);

        userEntity = changePassword(userEntity , password);

        return userConverter.convertToDto(userEntity);
    }

    @Override
    public List<UserDto> list(int max, int offset, String direction, String property) {
        property = ConvertUtil.getValidProperty(property, "id", "email", "enabled", "expire", "accountNonExpired", "accountNonLocked");
        Pageable pageable = ConvertUtil.createOffsetPageRequest(max, offset, direction, property);
        return userConverter.convertToDtoList(userRepository.findAllUsers(pageable));
    }

    @Override
    public List<UserDto> filter(UserFilterDto filterDTO) {

        UserSpecification specification = new UserSpecification(filterDTO);

        String property = ConvertUtil.getValidProperty(filterDTO.getProperty(), "id", "email" , "enabled" , "accountNonLocked");

        Pageable pageable = ConvertUtil.createOffsetPageRequest(filterDTO.getMax(), filterDTO.getOffset(), filterDTO.getDirection(), property);

        return userConverter.convertPageToDtoList(userRepository.findAll(specification, pageable));

    }

    @Override
    public long countFilter(UserFilterDto filterDTO) {
        UserSpecification specification = new UserSpecification(filterDTO);

        return userRepository.count(specification);
    }

    @Override
    public long count() {
        return userRepository.count();
    }
}
