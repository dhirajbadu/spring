package org.account.core.modal.entity;

import org.account.core.enumconstants.Permission;

import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Table;
import java.util.Set;
import org.account.core.enumconstants.Status;

@Entity
@Table(name = "table_role")
public class RoleEntity extends AbstractEntity<Long>{

    private String title;

    private Status status;

    @ElementCollection(fetch= FetchType.EAGER)
    private Set<Permission> permissionSet;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public Set<Permission> getPermissionSet() {
        return permissionSet;
    }

    public void setPermissionSet(Set<Permission> permissionSet) {
        this.permissionSet = permissionSet;
    }
}
