package org.account.core.modal.entity;

import org.account.core.enumconstants.UserType;

import javax.persistence.*;
import javax.validation.constraints.Email;
import java.util.Date;
import java.util.Set;

@Entity
@Table(name = "table_user")
public class UserEntity extends AbstractEntity<Long>{

    @Column(unique = true , nullable = false , length = 100)
    @Email
    private String email;

    @Column(nullable = false , length = 100)
    private String password;

    private boolean enabled;

    @Temporal(TemporalType.TIMESTAMP)
    private Date expire;

    private boolean accountNonExpired;

    private boolean accountNonLocked;

    @ManyToMany(fetch = FetchType.LAZY)
    private Set<RoleEntity> roleSet;

    private UserType userType;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public Date getExpire() {
        return expire;
    }

    public void setExpire(Date expire) {
        this.expire = expire;
    }

    public boolean isAccountNonExpired() {
        return accountNonExpired;
    }

    public void setAccountNonExpired(boolean accountNonExpired) {
        this.accountNonExpired = accountNonExpired;
    }

    public boolean isAccountNonLocked() {
        return accountNonLocked;
    }

    public void setAccountNonLocked(boolean accountNonLocked) {
        this.accountNonLocked = accountNonLocked;
    }

    public Set<RoleEntity> getRoleSet() {
        return roleSet;
    }

    public void setRoleSet(Set<RoleEntity> roleSet) {
        this.roleSet = roleSet;
    }

    public UserType getUserType() {
        return userType;
    }

    public void setUserType(UserType userType) {
        this.userType = userType;
    }
}
