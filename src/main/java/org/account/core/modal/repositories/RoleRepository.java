package org.account.core.modal.repositories;

import org.account.core.modal.entity.RoleEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RoleRepository extends JpaRepository<RoleEntity , Long>{

    RoleEntity findByTitle(String title);

    RoleEntity findById(long roleId);
}
