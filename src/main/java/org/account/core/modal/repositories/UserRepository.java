package org.account.core.modal.repositories;

import org.account.core.modal.entity.UserEntity;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserRepository extends JpaRepository<UserEntity , Long> , JpaSpecificationExecutor<UserEntity>{

    @Query("select u from UserEntity u join fetch u.roleSet where u.email = ?1")
    UserEntity findByUsernameWithRole(String email);

    UserEntity findByEmail(String email);

    UserEntity findById(long userId);

    @Query("select u from UserEntity u join fetch u.roleSet where u.id = ?1")
    UserEntity findByIdJoinRole(long userId);

    @Query("select u from UserEntity u")
    List<UserEntity> findAllUsers(Pageable pageable);
}
