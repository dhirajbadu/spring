package org.account.core.modal.converter;

import org.account.base.ConverterBase;
import org.account.commonUtls.DateParseUtil;
import org.account.core.modal.dto.UserDto;
import org.account.core.modal.entity.RoleEntity;
import org.account.core.modal.entity.UserEntity;
import org.account.core.modal.repositories.RoleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Service
public class UserConverter extends ConverterBase<UserEntity , UserDto>{

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private RoleRepository roleRepository;

    @Override
    public UserEntity convertToEntity(UserDto dto) {

        if (dto == null){
            return super.convertToEntity(dto);
        }

        UserEntity entity = copyConvertToEntity(dto , new UserEntity());

        entity.setEmail(trimString(dto.getEmail()).toLowerCase());
        entity.setPassword(passwordEncoder.encode(trimString(dto.getPassword())));

        return entity;
    }

    @Override
    public UserDto convertToDto(UserEntity entity) {

        if (entity == null) {
            return super.convertToDto(entity);
        }

        UserDto dto = new UserDto();

        dto.setUserId(entity.getId());
        dto.setVersion(entity.getVersion());
        dto.setCreated(entity.getCreated());
        dto.setExpire(entity.getExpire());
        dto.setAccountNonLocked(entity.isAccountNonLocked());
        dto.setEmail(entity.getEmail());
        dto.setAccountNonExpired(entity.isAccountNonExpired());
        dto.setEnabled(entity.isEnabled());
        dto.setUserType(entity.getUserType());
        dto.setPassword(entity.getPassword());

        return dto;
    }

    @Override
    public UserEntity copyConvertToEntity(UserDto dto, UserEntity entity) {
        
        if (entity == null || dto == null){
            super.copyConvertToEntity(dto , entity);
        }

        
        entity.setAccountNonExpired(dto.isAccountNonExpired());
        entity.setAccountNonLocked(dto.isAccountNonLocked());
        entity.setEnabled(dto.isEnabled());
        entity.setUserType(dto.getUserType());
        entity.setExpire(DateParseUtil.addYears(DateParseUtil.getCurrentDateTime() , 1));

        Set<RoleEntity> roleEntities = new HashSet<>();

        roleEntities.add(roleRepository.findByTitle("System"));

        entity.setRoleSet(roleEntities);
        
        return entity;
    }

    @Override
    public List<UserDto> convertToDtoList(List<UserEntity> entities) {
        return super.convertToDtoList(entities);
    }

    @Override
    public List<UserEntity> convertToEntityList(List<UserDto> dtoList) {
        return super.convertToEntityList(dtoList);
    }

    @Override
    public List<UserDto> convertPageToDtoList(Page<UserEntity> entities) {
        return super.convertPageToDtoList(entities);
    }
}
