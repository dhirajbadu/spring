package org.account.core.modal.dto.filterdto;

import org.account.base.AbstractFilterDto;

public class UserFilterDto extends AbstractFilterDto{

    private String email;

    public UserFilterDto(){
        super.setClass(this.getClass());
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
