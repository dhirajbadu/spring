package org.account.core.modal.dto;

import org.account.base.AbstractDto;
import org.account.core.enumconstants.Permission;
import org.account.core.enumconstants.Status;

import java.util.Set;

public class RoleDto extends AbstractDto {

    private String title;

    private Status status;

    private Set<Permission> permissionSet;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public Set<Permission> getPermissionSet() {
        return permissionSet;
    }

    public void setPermissionSet(Set<Permission> permissionSet) {
        this.permissionSet = permissionSet;
    }
}
