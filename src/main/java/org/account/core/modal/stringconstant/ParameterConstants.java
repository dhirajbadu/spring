package org.account.core.modal.stringconstant;

public class ParameterConstants {

    public static final int accountNumberLength = 6;
    public static final String customerAccountPrefix = "cust-";
    public static final String vendorAccountPrefix = "vend-";

}
