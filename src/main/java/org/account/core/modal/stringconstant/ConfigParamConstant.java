package org.account.core.modal.stringconstant;

public class ConfigParamConstant {

    public static final String repositoryPackagePath = "org.account.core.modal.repositories";
    public static final String entityPackagePath = "org.account.core.modal.entity";
    public static final String moduleBasePackagePath = "org.account";
    public static final String controllerPackagePath = "org.account.web.controller";
}
