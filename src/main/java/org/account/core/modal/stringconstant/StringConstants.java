package org.account.core.modal.stringconstant;

public class StringConstants {

    public static final String LOGIN_ERROR = "loginError";
    public static final String LOGIN_MESSAGE = "loginMessage";
    public static final String MESSAGE = "message";
    public static final String ERROR = "error";

    public static final String USER_TYPE_LIST = "userTypeList";
    public static final String USER = "user";
}
