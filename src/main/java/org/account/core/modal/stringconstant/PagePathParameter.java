package org.account.core.modal.stringconstant;

public class PagePathParameter {

    public static final String USER_LIST = "/user/list";
    public static final String USER_ADD = "/user/add";
    public static final String USER_SHOW = "/user/show";
    public static final String USER_EDIT = "/user/edit";
    public static final String USER_CHANGEPASSWORD = "/user/changepassword";
}
