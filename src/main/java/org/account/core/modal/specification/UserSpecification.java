package org.account.core.modal.specification;

import org.account.base.SpecificationBase;
import org.account.commonUtls.ParseUtls;
import org.account.core.modal.dto.filterdto.UserFilterDto;
import org.account.core.modal.entity.UserEntity;

import javax.persistence.criteria.*;
import java.util.ArrayList;
import java.util.List;

public class UserSpecification extends SpecificationBase<UserFilterDto , UserEntity>{

    public UserSpecification(UserFilterDto filterDTO) {
        super(filterDTO);
    }

    @Override
    public Predicate toPredicate(Root<UserEntity> root, CriteriaQuery<?> query, CriteriaBuilder cb) {

        final List<Predicate> predicates = new ArrayList<Predicate>();

        if (ParseUtls.isNull(filterDTO.getQuery())) {

            if (ParseUtls.isNotNull(filterDTO.getEmail())) {
                predicates.add(cb.like(root.get("email"), "%" + filterDTO.getEmail() + "%"));
            }

        }else {

            if (ParseUtls.isNotNull(filterDTO.getQuery())) {
                predicates.add(cb.like(root.get("email"), "%" + filterDTO.getQuery() + "%"));
            }

        }

        return cb.and(predicates.toArray(new Predicate[]{}));
    }
}
