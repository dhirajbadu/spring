package org.account.base;

import org.account.core.enumconstants.Status;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.List;

public class SpecificationBase<D,E> implements Specification<E> {

    protected D filterDTO;

    public SpecificationBase(){
        super();
    }

    public SpecificationBase(D filterDTO){
        super();
        this.filterDTO = filterDTO;
    }

    @Override
    public Predicate toPredicate(Root<E> root, CriteriaQuery<?> query, CriteriaBuilder cb) {

        final List<Predicate> predicates = new ArrayList<Predicate>();

        predicates.add(cb.equal(root.get("status"), Status.ACTIVE));

        return cb.and(predicates.toArray(new Predicate[]{}));
    }

}
