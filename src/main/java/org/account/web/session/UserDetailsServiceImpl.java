package org.account.web.session;


import org.account.commonUtls.LoggerUtil;
import org.account.core.enumconstants.Permission;
import org.account.core.modal.dto.UserDto;
import org.account.core.modal.entity.RoleEntity;
import org.account.core.modal.entity.UserEntity;
import org.account.core.modal.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Service
public class UserDetailsServiceImpl implements UserDetailsService {

    @Autowired
    private UserRepository userRepository;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

        try {


            UserEntity u = userRepository.findByUsernameWithRole(username.toLowerCase());

            System.out.println("login");

            //todo
            /*HttpServletRequest curRequest = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();

            final StringBuilder msg = new StringBuilder();

            msg.append(curRequest.getRemoteAddr());

            final String forwardedFor = curRequest.getHeader("X-Forwarded-For");

            if (forwardedFor != null) {
                msg.append(", forwardedFor = ").append(forwardedFor);
            }*/

            if (u == null) {

                throw new UsernameNotFoundException("user doesnt exists");
            }

            UserDto dto = getUserDTO(u);

            return new UserDetailsWrapper(dto, getAuthorities(u));

        } catch (UsernameNotFoundException ex){
            LoggerUtil.logInfo("unregistered user trying to login : \t username : " + username);
            throw new UsernameNotFoundException("user doesnt exists");
        } catch (Exception e) {
            LoggerUtil.logException(this.getClass(), e);
            throw new UsernameNotFoundException("user doesnt exists");
        }

    }

    private List<GrantedAuthority> getAuthorities(UserEntity user) {

        Set<String> authorities = new HashSet<>();

        for (RoleEntity role : user.getRoleSet()) {

            for (Permission permission : role.getPermissionSet()) {

                try {

                    authorities.add(permission.getValue());

                } catch (Exception e) {
                    continue;
                }
            }
        }

        List<GrantedAuthority> auth = createAuthorityList(authorities);

        if (auth == null) {

            auth = new ArrayList<GrantedAuthority>();

            auth.add(new SimpleGrantedAuthority(user.getUserType().getValue()));
            auth.add(new SimpleGrantedAuthority("AUTHENTICATED"));

        } else {

            auth.add(new SimpleGrantedAuthority(user.getUserType().getValue()));
            auth.add(new SimpleGrantedAuthority("AUTHENTICATED"));
        }

        return auth;
    }

    private List<GrantedAuthority> createAuthorityList(Set<String> roles) {

        List<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>(roles.size());

        for (String role : roles) {

            authorities.add(new SimpleGrantedAuthority(role));
        }

        return authorities;
    }

    private UserDto getUserDTO(UserEntity user) {

        UserDto dto = new UserDto();

        dto.setUserId(user.getId());
        dto.setEnabled(user.isEnabled());
        dto.setEmail(user.getEmail());
        dto.setPassword(user.getPassword());
        dto.setUserType(user.getUserType());
        dto.setAccountNonExpired(user.isAccountNonExpired());
        dto.setAccountNonLocked(user.isAccountNonLocked());
        dto.setExpire(user.getExpire());

        return dto;
    }


}
