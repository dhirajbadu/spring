package org.account.web.controller;

import com.sun.javafx.binding.StringConstant;
import org.account.core.modal.stringconstant.StringConstants;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;

@Controller
public class HomeController {

    @GetMapping("/")
    public String index(){

        return "redirect:/dashboard";
    }

    @GetMapping("/login")
    public String loginGet(@RequestParam(value = "loginError" , required = false) String loginError , @RequestParam(value = "loginMessage" , required = false) String message , ModelMap map , HttpServletRequest req){

        if (loginError == null){
            loginError = (String) req.getAttribute("loginError");
        }

        if (message == null){
            message = (String) req.getAttribute("loginMessage");
        }
        map.put(StringConstants.LOGIN_ERROR , loginError);
        map.put(StringConstants.LOGIN_MESSAGE, message);
        return "login";
    }

    @GetMapping("/dashboard")
    public String dashboard(){
        return "/dashboard/dashboard";
    }

    @PostMapping("/login")
    public String loginPost(@RequestParam(value = "loginError" , required = false) String loginError , @RequestParam(value = "loginMessage" , required = false) String message , ModelMap map , HttpServletRequest req){

        if (loginError == null){
            loginError = (String) req.getAttribute("loginError");
        }

        map.put(StringConstants.LOGIN_ERROR , loginError);
        if (message == null){
            message = (String) req.getAttribute("loginMessage");
        }
        map.put(StringConstants.LOGIN_ERROR , loginError);
        map.put(StringConstants.LOGIN_MESSAGE, message);
        return "login";
    }
}
