package org.account.web.controller.rest;

import org.account.base.FilterMapper;
import org.account.commonUtls.LoggerUtil;
import org.account.core.modal.dto.UserDto;
import org.account.core.modal.dto.filterdto.UserFilterDto;
import org.account.core.service.iservices.IUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/api/user")
public class UserRestController {

    @Autowired
    private IUserService userService;

    @GetMapping("/list")
    public Map<String , Object> ajaxList(HttpServletRequest req) {
        try {

            Map<String, Object> resultMap = new HashMap<>();

            UserFilterDto userFilterDto = (UserFilterDto) new UserFilterDto().build(req);
            List<UserDto> userDtoList = userService.filter(userFilterDto);
            List<String[]> arrStr = new ArrayList<>();

            for (UserDto userDto : userDtoList) {
                String[] arr = new String[6];

                arr[0] = userDto.getUserId().toString();
                arr[1] = userDto.getEmail();
                arr[2] = userDto.isEnabled() ? "true" : "false";
                arr[3] = userDto.isAccountNonLocked() ? "true" : "false";
                arr[4] = userDto.isAccountNonExpired() ? "true" : "false";
                arr[5] = userDto.getExpire().toString();

                arrStr.add(arr);

            }

            resultMap.put("data", arrStr);
            resultMap.put("recordsTotal", userService.count());
            resultMap.put("recordsFiltered", userService.countFilter(userFilterDto));

            return resultMap;
        }catch (Exception e){
            LoggerUtil.logException(this.getClass(), e);
            e.printStackTrace();
            throw e;
        }
    }

}
