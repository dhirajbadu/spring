package org.account.web.controller;

import org.account.commonUtls.AuthenticationUtil;
import org.account.commonUtls.LoggerUtil;
import org.account.core.enumconstants.UserType;
import org.account.core.modal.dto.UserDto;
import org.account.core.modal.stringconstant.PagePathParameter;
import org.account.core.modal.stringconstant.StringConstants;
import org.account.core.service.iservices.IUserService;
import org.account.core.service.serviceimpl.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Controller
@RequestMapping("/user")
public class UserController {

    @Autowired
    private IUserService userService;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @GetMapping("/list")
    public String list(){

        return PagePathParameter.USER_LIST;
    }

    @GetMapping("/add")
    public String add(ModelMap map){

        map.put(StringConstants.USER_TYPE_LIST , UserType.values());
        return PagePathParameter.USER_ADD;
    }

    @GetMapping("/show/{userId}")
    public String show(@PathVariable("userId")Long userId , ModelMap map , RedirectAttributes redirectAttributes){

        try {

            if (userId == null) {
                redirectAttributes.addFlashAttribute(StringConstants.ERROR, "required parameter not found");
                return "redirect:/user/list";
            }

            if (userId < 0) {
                redirectAttributes.addFlashAttribute(StringConstants.ERROR, "invalid parameter");
                return "redirect:/user/list";
            }

            UserDto userDto = userService.getById(userId);

            if (userDto == null) {
                redirectAttributes.addFlashAttribute(StringConstants.ERROR, "user details not found");
                return "redirect:/user/list";
            }

            map.put(StringConstants.USER, userDto);
            return PagePathParameter.USER_SHOW;
        } catch (Exception e) {

            LoggerUtil.logException(this.getClass(), e);

            throw e;
        }
    }

    @GetMapping("/edit/{userId}")
    public String edit(@PathVariable("userId")Long userId , ModelMap map , RedirectAttributes redirectAttributes){

        try {

            if (userId == null) {
                redirectAttributes.addFlashAttribute(StringConstants.ERROR, "required parameter not found");
                return "redirect:/user/list";
            }

            if (userId < 0) {
                redirectAttributes.addFlashAttribute(StringConstants.ERROR, "invalid parameter");
                return "redirect:/user/list";
            }

            UserDto userDto = userService.getById(userId);

            if (userDto == null) {
                redirectAttributes.addFlashAttribute(StringConstants.ERROR, "user details not found");
                return "redirect:/user/list";
            }

            map.put(StringConstants.USER_TYPE_LIST , UserType.values());
            map.put(StringConstants.USER, userDto);
            return PagePathParameter.USER_EDIT;
        } catch (Exception e) {

            LoggerUtil.logException(this.getClass(), e);

            throw e;
        }
    }

    @PostMapping("/save")
    public String save(@ModelAttribute("user") UserDto userDto, BindingResult result, ModelMap modelMap, RedirectAttributes redirectAttributes){


        try {

            synchronized (this.getClass()) {

               // UserError error = userValidation.onSave(userDTO, result);

                /*if (!error.isValid()) {

                    modelMap.put(StringConstants.ROLE_LIST,
                            roleApi.list(Status.ACTIVE));
                    modelMap.put(StringConstants.USER, userDTO);
                    modelMap.put(StringConstants.USER_TYPE_LIST,
                            UserType.values());
                    modelMap.put(StringConstants.USER_ERROR, error);
                    modelMap.put(StringConstants.BRANCH_LIST,
                            branchApi.list(Status.ACTIVE, 0, 300));

                    return "user/add";
                }*/

                userDto = userService.save(userDto);

                redirectAttributes.addFlashAttribute(StringConstants.MESSAGE,
                        "user created successfully");
            }

        } catch (Exception e) {

            LoggerUtil.logException(this.getClass(), e);

            throw e;
        }
        return "redirect:/user/list";
    }

    @PostMapping("/update")
    public String update(@ModelAttribute("user") UserDto userDto, BindingResult result, ModelMap modelMap, RedirectAttributes redirectAttributes){


        try {

            synchronized (this.getClass()) {

                // UserError error = userValidation.onSave(userDTO, result);

                /*if (!error.isValid()) {

                    modelMap.put(StringConstants.ROLE_LIST,
                            roleApi.list(Status.ACTIVE));
                    modelMap.put(StringConstants.USER, userDTO);
                    modelMap.put(StringConstants.USER_TYPE_LIST,
                            UserType.values());
                    modelMap.put(StringConstants.USER_ERROR, error);
                    modelMap.put(StringConstants.BRANCH_LIST,
                            branchApi.list(Status.ACTIVE, 0, 300));

                    return "user/add";
                }*/

                userDto = userService.update(userDto);

                redirectAttributes.addFlashAttribute(StringConstants.MESSAGE,
                        "user updated successfully");
            }

        } catch (Exception e) {

            LoggerUtil.logException(this.getClass(), e);

            throw e;
        }
        return "redirect:/user/list";
    }

    @GetMapping("/changepassword")
    public String changePassword() {

        return PagePathParameter.USER_CHANGEPASSWORD;
    }

    @PostMapping("/updatepassword")
    public String updatePassword(RedirectAttributes redirectAttributes,
                                 @RequestParam("oldpassword") String oldpassword,
                                 @RequestParam("newpassword") String newpassword,
                                 @RequestParam("repassword") String repassword,
                                 HttpServletRequest request, HttpServletResponse response)
            throws IOException {

        if (oldpassword == null || newpassword == null || repassword == null) {
            redirectAttributes.addFlashAttribute(StringConstants.ERROR, "please fill the full form");
            return "redirect:/user/changepassword";
        }

        if ("".equals(oldpassword.trim()) || "".equals(newpassword.trim()) || "".equals(repassword.trim())) {
            redirectAttributes.addFlashAttribute(StringConstants.ERROR, "please fill the full form");
            return "redirect:/user/changepassword";
        }

        UserDto currentUser = AuthenticationUtil.getCurrentUser();

        UserDto userDto = userService.getById(currentUser.getUserId());

        if (!passwordEncoder.matches(oldpassword, userDto.getPassword())) {
            redirectAttributes.addFlashAttribute(StringConstants.ERROR, "invalid current password");
            return "redirect:/user/changepassword";
        }

        if (!newpassword.equals(repassword)) {
            redirectAttributes.addFlashAttribute(StringConstants.ERROR, "new password did not matched with confirm password");
            return "redirect:/user/changepassword";
        }

        if (newpassword.trim().length() < 5) {
            redirectAttributes.addFlashAttribute(StringConstants.ERROR, "new password must be greater than 5 length");
            return "redirect:/user/changepassword";
        }

        try {
            userService.changePassword(currentUser.getUserId(), newpassword, request, response);
            redirectAttributes.addFlashAttribute(StringConstants.MESSAGE, "password changed successfully");

        } catch (Exception e) {
            LoggerUtil.logException(this.getClass(), e);

            throw e;
        }

        return "redirect:/login";
    }
}
