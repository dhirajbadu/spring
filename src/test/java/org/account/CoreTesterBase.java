package org.account;

import org.account.config.PersistenceJPAConfig;
import org.account.config.WebMvcConfig;
import org.junit.Before;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

//classes
//@RunWith(SpringRunner.class)
/*@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = AppInitializer.class)*/
@WebAppConfiguration
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {WebMvcConfig.class , PersistenceJPAConfig.class})
public class CoreTesterBase {

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
    }
}
