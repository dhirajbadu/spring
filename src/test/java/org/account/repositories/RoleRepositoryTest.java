package org.account.repositories;

import org.account.CoreTesterBase;
import org.account.core.enumconstants.Permission;
import org.account.core.enumconstants.Status;
import org.account.core.modal.entity.RoleEntity;
import org.account.core.modal.repositories.RoleRepository;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.HashSet;
import java.util.Set;

public class RoleRepositoryTest extends CoreTesterBase{

    @Autowired
    private RoleRepository roleRepository;

    @Test
    public void save(){
        RoleEntity roleEntity = new RoleEntity();

        Set<Permission> permissionSet = new HashSet<>();

        permissionSet.add(Permission.USER_CREATE);

        roleEntity.setTitle("Test");
        roleEntity.setStatus(Status.ACTIVE);
        roleEntity.setPermissionSet(permissionSet);

        roleEntity = roleRepository.save(roleEntity);

        System.out.println(roleEntity.getId());
    }
}
